<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login');
Route::post('/check-token', 'AuthController@checkToken');

Route::middleware('checktoken')->namespace('Data')->group(function() {
    //仓库
    Route::prefix('house')->group(function() {
        Route::get('index', 'HouseController@index');
    });

    //货物
    Route::prefix('good')->group(function() {
        Route::post('create', 'GoodController@create');
    });
});
