<?php

return [
    'appid' => env('WECHAT_ID'),
    'appsecret' => env('WECHAT_SECRET'),
    'loginurl' => 'https://api.weixin.qq.com/sns/jscode2session',
];
