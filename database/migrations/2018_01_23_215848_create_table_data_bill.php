<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDataBill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_bill', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buyer_id');
            $table->float('income', 8, 2);
            $table->float('prime', 8, 2);
            $table->timestamp('create')->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_bill');
    }
}
