<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBillList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_bill_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_id');
            $table->integer('house_id');
            $table->integer('good_id');
            $table->integer('good_number');
            $table->float('good_price', 8, 2);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_bill_list');
    }
}
