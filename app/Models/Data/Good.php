<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;

class Good extends BaseModel
{
    protected $table = 'data_good';
}
