<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;

class House extends BaseModel
{
    protected $table = 'data_house';
}
