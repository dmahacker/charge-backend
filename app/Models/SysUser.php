<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SysUser extends Model
{
    protected $table = 'sys_user';

    public function generateToken() {
        do {
            $token = "";
            for($i=0; $i<32; $i++) {
                $token .= dechex(rand(0,15));
            }
            $exist = self::where('token', $token)->first();
        } while($exist);
        $this->token = $token;
        return $this;
    }

    public static function checkToken($token) {
        $user = self::where('token', $token)->first();
        if(!$user) {
            return null;
        }
        $now = new Carbon();
        $expire = $user->updated_at->diffInMinutes($now);
        if($expire > 120) {
            return null;
        }
        return $user;
    }
}
