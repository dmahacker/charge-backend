<?php

namespace App\Services;

class Response
{
    const RESPONSE_SUCCESS = 1000;
    const RESPONSE_LOGIN_FAIL = 1001;
    const RESPONSE_TOKEN_INVALID = 1002;
    const RESPONSE_FORM_INVALID = 1003;

    public static function make($code, $data = [], $msg = '') {
        return compact('code', 'data', 'msg');
    }
}
