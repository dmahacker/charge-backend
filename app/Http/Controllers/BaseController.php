<?php

namespace App\Http\Controllers;

use Validator;
use App\Services\Response;
use Illuminate\Http\Request;

class BaseController extends Controller
{

    public function success($data = [], $msg = '') {
        $code = Response::RESPONSE_SUCCESS;
        return $this->response($code, $data, $msg);
    }

    public function error($code, $data = [], $msg = '') {
        return $this->response($code, $data, $msg);
    }

    public function response($code, $data = [], $msg = '') {
        return Response::make($code, $data, $msg);
    }

    public function validation($rule = [], $custom = [],$callback = null) {
        if(is_callable($custom)) {
            $callback = $custom;
            $custom = [];
        } else if(is_callable($rule)) {
            $callback = $rule;
            $rule = [];
        }
        //待完善对custom的处理
        $input = \Request::all();
        $validator = Validator::make($input, $rule, config('validation'));
        if($validator->fails()) {
            $errors = $validator->errors();
            return $this->response(Response::RESPONSE_FORM_INVALID, $errors, 'Form invalid');
        }
        if(is_callable($callback)) {
            $data = $callback($input);
        } else {
            $data = [];
        }
        return $this->success($data);
    }
}
