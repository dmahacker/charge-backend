<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Models\Data\House;
use App\Http\Controllers\BaseController;

class HouseController extends BaseController
{
    public function index(Request $request) {
        $user = $request->get('user');
        $houses = House::where('user_id', $user->id)
            ->get();
        return $this->success($houses);
    }
}
