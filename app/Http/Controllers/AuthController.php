<?php

namespace App\Http\Controllers;

use Curl\Curl;
use Carbon\Carbon;
use App\Models\SysUser;
use App\Services\Response;
use Illuminate\Http\Request;

class AuthController extends BaseController
{
    public function login(Request $request) {
        $code = $request->get('code', '');
        $curl = new Curl();
        $curl->get(config('wechat.loginurl'), [
            'appid' => config('wechat.appid'),
            'secret' => config('wechat.appsecret'),
            'js_code' => $code,
            'grant_type' => 'authorization_code',
        ]);
        $response = json_decode($curl->response);

        //如果访问api错误
        if(isset($response->errcode)) {
            return $this->error(
                Response::RESPONSE_LOGIN_FAIL,
                [ 'errcode' => $response->errcode ],
                $response->errmsg
            );
        }

        //如果对应openid用户不存在则新建用户
        $user = SysUser::where('openid', $response->openid)->first();
        if(!$user) {
            $user = new SysUser();
            $user->openid = $response->openid;
            $user->save();
        }

        //更新用户的数据
        $user->session_key = $response->session_key;
        $user->updated_at = new Carbon();
        $user->generateToken();
        $user->save();

        return $this->success($user->token);
    }

    public function checkToken(Request $request) {
        $token = $request->get('token', '');
        $user = SysUser::checkToken($token);
        if(!$user) {
            return $this->error(
                Response::RESPONSE_TOKEN_INVALID,
                [],
                'Token invalid'
            );
        } else {
            return $this->success($user);
        }
    }
}
