<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\SysUser;
use App\Services\Response;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->get('token');
        $user = SysUser::checkToken($token);
        if(!$user) {
            abort(401, 'Token 已过期');
        }
        $request->offsetUnset('token');
        $request->offsetSet('user', $user);
        return $next($request);
    }
}
